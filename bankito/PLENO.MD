# DESAFIO BANKITO - FRONT ANGULAR #

## Preparado?

Que legal que você topou esse desafio DEV! Queremos conhecer melhor suas habilidades na prática. Então vamos lá para o teste!

## Bankito

O Bankito é uma startup que foi criada para de ser um banco simples e minimalista,
com funcionalidades diretas e acertivas, mostrando para o usuário somente o que interessa.

Com base nesse propósito ele se baseia em apenas uma funcionalidade: **Transferências**.

E é assim que iremos mudar o mercado de bancos digitais e pagamentos.
Mas para isso precisamos que você coloque em prática seus conhecimentos aprendidos até aqui em layout e programação para completar esse desafio.

Além do layout você deverá integrar o app com a API que já foi implementada nas primeiras sprints porque foi priorizada pelo P.O no backlog como de estrema importância.

## O que será avaliado

* Sua habilidade em contruir o layout em relação ao protótipo, respeitando posicionamentos, espaçamentos e cores dos elementos
* Organização de CSS
* Organização e lógica do seu código
* Tipagem do código (evite utilização de tipos **any** ou **unknown**)
* Organização e divisão de componentes
* Organização de pastas e módulos
* Lazy loading

## O que deve ser implementado?

Alguns itens estão marcados onde a implementação não é obrigatória e sua implementação será considerado um extra!

Abaixo também segue algumas explicações a cerca de cada implementação para complementar o fluxo do protótipo.

1. Tela de Splash screen (Extra)
2. Tela de Start screen (Extra)
3. Tela de Login
      * Informa CPF e SENHA para realizar o login
      * CPF e SENHA da conta de testes será informada na documentação da API
      * Texto "Não consigo acessar" implementação apenas do layout, não deve ser implementada nenhuma funcionalidade
4. Tela da Home
      * Usuário pode consultar seus saldos (entradas, saídas e saldo total)
      * Pode realizar uma nova transferência
      * Consultar o extrato das transações
      * Filtrar o extrato com as seguintes opções
        * Desde o início (opção default)
        * Este mês
        * 3 Meses
        * 1 Ano
      * Sair do App
5. Nova transferência
      * Usuário digita o valor da transferência
      * Informa o número da conta
      * Após ter os dois valores busca na api os dados utilizando o número da conta e exibe as informações de resumo
      * Após isso pode realizar a transferência
6. Animações de transição de tela (Extra)
7. Integrar com a API

## Regras da implementação

* O projeto deverá ser implementado utilizando a última versão disponível do Angular
* Esperamos que não seja feito o uso de nenhum tipo de framework de CSS, GRID LAYOUT, Angular Material ou Boostrap, queremos ver suas habilidades em implementar o layout da forma mais pura
* Realizar o upload do projeto no Stack Blitz, não se esqueça de testar o projeto por lá para garantir que nada foi quebrado
* Versionar o projeto utilizando Git e subir em alguma hospedagem de sua preferência Git Hub ou Bitbuket. Só não se esqueça de deixar o respositório público
* Quando finalizar a implementação enviar os links de acesso Stack Blitz e repositório via email para quem lhe enviou o desafio

## Protótipo

O protótipo foi criado no Figma, se você nunca utilizou, basta apenas criar uma conta e ele permitirá que você veja o protótipo em modo de editor, onde poderá ver medidas e cores dos elementos por exemplo.

[Link Protótipo](https://www.figma.com/proto/GjnmJfpBN14rMhVbzDPLuI/Bankito?node-id=4%3A5&scaling=scale-down)
## Integração com API

Acesse o link da documentação da api para saber como instalar e realizar as integrações.

[Link API]()

## Dúvidas ou erros

Se encontrar erros ou tiver dúvidas não deixe de entrar em contato, comunique quem lhe enviou o desafio.